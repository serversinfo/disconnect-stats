#include <hlstatsX_adv>
#pragma newdecls required
#define DEBUG
#if defined DEBUG
stock void debugMessage(const char[] message, any ...)
{
	char szMessage[256], szPath[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, szPath, sizeof(szPath), "logs/disconnects_stats.txt");
	VFormat(szMessage, sizeof(szMessage), message, 2);
	LogToFile(szPath, szMessage);
}
#define dbgMsg(%0) debugMessage(%0)
#else
#define dbgMsg(%0)
#endif

int map_st_time
int map_end_time
int rnd_st_time
int pl_dth_time[MAXPLAYERS+1]
char disconn_reason[MAXPLAYERS+1]
bool EndMatch;

public void OnPluginStart() 
{
	dbgMsg("Запуск плагина disconnects_stats");
	HookEvent("round_start",		eV_OnRoundStart,		EventHookMode_PostNoCopy);
	HookEvent("player_death",		eV_OnPlayerDeath,		EventHookMode_PostNoCopy);
	HookEvent("player_spawned",		eV_OnPlayerSpawn,		EventHookMode_Post);
	//HookEvent("player_connect",	eV_OnPlayerConnect,		EventHookMode_PostNoCopy);
	HookEvent("player_disconnect",	eV_OnPlayerDisconnect,	EventHookMode_Post);
	HookEvent("cs_win_panel_match",	eV_EndMatch,			EventHookMode_PostNoCopy);
}

public void OnMapStart()
{
	map_st_time = GetTime();
	map_end_time = 0;
	rnd_st_time = 0;
	
	char mapname[64];
	GetCurrentMap(mapname,sizeof(mapname))
	dbgMsg("Map %s", mapname);
	EndMatch = false;
}

public void OnMapEnd()
{
	map_end_time = GetTime();
	PrintToChatAll("OnMapEnd");
	dbgMsg("MapEnd");
	EndMatch = false;
}

//public bool OnClientConnect(client, String:rejectmsg[], maxlen)
//{
//	if (!IsFakeClient(client))
//		dbgMsg("%N OnClientConnect", client);
//	return true;
//}

public void OnClientConnected(int client)
{
	pl_dth_time[client]= -1; 
	//if (!IsFakeClient(client))
	//	dbgMsg("%N подключился", client);
}

public Action eV_OnRoundStart (Handle event, const char[] name, bool db)
{
	rnd_st_time = GetTime();
	EndMatch = false;
}

public Action eV_EndMatch (Handle event, const char[] name, bool db)
{
	PrintToChatAll("eV_EndMatch");
	dbgMsg("eV_EndMatch");
	EndMatch = true;
}

public Action eV_OnPlayerSpawn (Handle event, const char[] name, bool db)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	if (client != 0)
		pl_dth_time[client]= -1;
}

public Action eV_OnPlayerDeath (Handle event, const char[] name, bool db)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	pl_dth_time[client] = GetTime();
}

public Action eV_OnPlayerDisconnect (Handle event, const char[] name, bool db)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	GetEventString(event, "reason", disconn_reason, sizeof(disconn_reason));
	
	if (!client)
		return Plugin_Continue;
	
	if (IsClientConnected(client)) {
		if(!IsFakeClient(client)) {
			dbgMsg("%N время сессии %i", client, RoundToFloor(GetClientTime(client)) );
			if(IsClientInGame(client)) {
				dbgMsg("%i всего наиграно", GetClientPlayedTime(client) );
				if (!IsPlayerAlive(client)) {
					if (pl_dth_time[client] < 1)
						dbgMsg("%N не живой и не умирал", client);
					else {
						int pl_dth_time_del = GetTime() - pl_dth_time[client];
						dbgMsg("%N вышел спустя %i сек спосле своей смерти", client, pl_dth_time_del);
					}
				}
			} else dbgMsg("%N вышел не успев подключиться", client);
			if (EndMatch)
				dbgMsg("%N вышел во время конца карты", client);
			if (!rnd_st_time)
				dbgMsg("%N вышел спустя %i сек после окончания карты до начала 1 раунда (%s)", client, GetTime() - map_end_time, disconn_reason);
			else dbgMsg("%N вышел спустя %i сек с начала раунда и %i сек с начала карты (%s)", client, GetTime() - rnd_st_time, GetTime() - map_st_time, disconn_reason);
		}
	} else dbgMsg("Клиент %i не присоединен", client);
	return Plugin_Continue;
}

public void OnClientDisconnect_Post(int client)		//не работает, на всякий случай написал, хз зачем
{
	if (IsClientConnected(client))
		if (!IsFakeClient(client))
			dbgMsg("%N вышел с сервера (post)", client);
}